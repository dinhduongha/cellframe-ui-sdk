import QtQuick 2.0
import "qrc:/"


Item
{
    ///@details dapMainFontTheme ID of item with all project fonts.
    property alias dapMainFontTheme: dapFontsObjects
    ///@details dapFactor Scaling factor.
    property int dapFactor: 1

    //Add Font Loader
    DapFont
    {
        id: dapFonts
        dapFontPath: "qrc:/resources/fonts/Quicksand/"
        dapFontNames: ["Quicksand-Bold.ttf",
                    "Quicksand-Light.ttf",
                    "Quicksand-Medium.ttf",
                    "Quicksand-Regular.ttf",     //3
                    "Quicksand-SemiBold.ttf",
                    "Quicksand-VariableFont_wght.ttf"]
    }
    QtObject
    {
        id:dapFontsObjects


        property font dapFontQuicksandBold14:                  Qt.font({
                                                                        family: dapFonts.dapProjectFonts[0].name,
                                                                        bold: true,
                                                                        italic: false,
                                                                        pixelSize: 14 * dapFactor
                                                                    })

        ///@details dapFontQuicksandLight10 Font of Quicksand font family (light, 10pt)
        property font dapFontQuicksandLight10:                 Qt.font({
                                                                        family: dapFonts.dapProjectFonts[1].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 10 * dapFactor
                                                                    })


        ///@details dapFontQuicksandLight12 Font of Quicksand font family (light, 12pt)
        property font dapFontQuicksandLight12:                 Qt.font({
                                                                        family: dapFonts.dapProjectFonts[1].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 12 * dapFactor
                                                                    })


        ///@details dapFontQuicksandLight14 Font of Quicksand font family (light, 14pt)
        property font dapFontQuicksandLight14:                 Qt.font({
                                                                        family: dapFonts.dapProjectFonts[1].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 14 * dapFactor
                                                                    })


        ///@details dapFontQuicksandLight16 Font of Quicksand font family (light, 16pt)
        property font dapFontQuicksandLight16:                 Qt.font({
                                                                        family: dapFonts.dapProjectFonts[1].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 16 * dapFactor
                                                                    })


        ///@details dapFontQuicksandLight27 Font of Quicksand font family (light, 27pt)
        property font dapFontQuicksandLight27:                 Qt.font({
                                                                        family: dapFonts.dapProjectFonts[1].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 27 * dapFactor
                                                                    })


        ///@details dapFontQuicksandightCustom Font of Quicksand font family (light, without parameters)
        property font dapFontQuicksandLightCustom:             Qt.font({   family: dapFonts.dapProjectFonts[1].name })


        property font dapFontQuicksandMedium10:                Qt.font({
                                                                        family: dapFonts.dapProjectFonts[2].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 10 * dapFactor
                                                                    })

        property font dapFontQuicksandMedium11:                Qt.font({
                                                                        family: dapFonts.dapProjectFonts[2].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 11 * dapFactor
                                                                    })

        property font dapFontQuicksandMedium12:                Qt.font({
                                                                        family: dapFonts.dapProjectFonts[2].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 12 * dapFactor
                                                                    })


        property font dapFontQuicksandMedium14:                Qt.font({
                                                                        family: dapFonts.dapProjectFonts[2].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 14 * dapFactor
                                                                    })


        property font dapFontQuicksandMedium16:                Qt.font({
                                                                        family: dapFonts.dapProjectFonts[2].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 16 * dapFactor
                                                                    })


        property font dapFontQuicksandMedium18:                Qt.font({
                                                                        family: dapFonts.dapProjectFonts[2].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 18 * dapFactor
                                                                    })


        property font dapFontQuicksandMedium26:                Qt.font({
                                                                        family: dapFonts.dapProjectFonts[2].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 26 * dapFactor
                                                                    })


        property font dapFontQuicksandMedium27:                Qt.font({
                                                                        family: dapFonts.dapProjectFonts[2].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 27 * dapFactor
                                                                    })

        property font dapFontQuicksandMediumBold16:            Qt.font({
                                                                        family: dapFonts.dapProjectFonts[2].name,
                                                                        bold: true,
                                                                        italic: false,
                                                                        pixelSize: 16 * dapFactor
                                                                    })


        property font dapFontQuicksandRegular10:               Qt.font({
                                                                        family: dapFonts.dapProjectFonts[3].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 10 * dapFactor
                                                                    })


        property font dapFontQuicksandRegular11:               Qt.font({
                                                                        family: dapFonts.dapProjectFonts[3].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 11 * dapFactor
                                                                    })

        property font dapFontQuicksandRegular12:               Qt.font({
                                                                        family: dapFonts.dapProjectFonts[3].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 12 * dapFactor
                                                                    })


        property font dapFontQuicksandRegular14:               Qt.font({
                                                                        family: dapFonts.dapProjectFonts[3].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 14 * dapFactor
                                                                    })

        property font dapFontQuicksandRegular16:               Qt.font({
                                                                        family: dapFonts.dapProjectFonts[3].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 16 * dapFactor
                                                                    })


        property font dapFontQuicksandRegular18:               Qt.font({
                                                                        family: dapFonts.dapProjectFonts[3].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 18 * dapFactor
                                                                    })


        property font dapFontQuicksandRegular28:               Qt.font({
                                                                        family: dapFonts.dapProjectFonts[3].name,
                                                                        bold: false,
                                                                        italic: false,
                                                                        pixelSize: 28 * dapFactor
                                                                    })

        property font dapFontQuicksandRegularCustom:           Qt.font({   family: dapFonts.dapProjectFonts[3].name })

    }


}  //root




