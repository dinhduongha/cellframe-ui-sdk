QT += core qml

INCLUDEPATH += $$PWD $$PWD/../

win32{
    QMAKE_CXXFLAGS +=  -mno-ms-bitfields
}

DISTFILES +=

HEADERS += \
    $$PWD/DapServiceClientMessage.h \
    $$PWD/DapSettingsSingletonProvider.h \
    $$PWD/DapSystemTrayIcon.h

SOURCES += \
    $$PWD/DapServiceClientMessage.cpp \
    $$PWD/DapSettingsSingletonProvider.cpp \
    $$PWD/DapSystemTrayIcon.cpp




