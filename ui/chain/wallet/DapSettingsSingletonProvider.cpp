#include "DapSettingsSingletonProvider.h"

DapSettingsSingletonProvider::DapSettingsSingletonProvider(QObject *parent) : DapSettings(parent)
{

}

/// Method that implements the singleton pattern for the qml layer.
/// @param engine QML application.
/// @param scriptEngine The QJSEngine class provides an environment for evaluating JavaScript code.
QObject *DapSettingsSingletonProvider::singletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return &getInstance();
}
