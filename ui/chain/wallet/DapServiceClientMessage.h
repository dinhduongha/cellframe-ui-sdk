#ifndef DAPSERVICECLIENTMESSAGE_H
#define DAPSERVICECLIENTMESSAGE_H

#include <serviceClient/DapServiceClient.h>
#include <QMessageBox>

class DapServiceClientMessage : public DapServiceClient
{
    Q_OBJECT
//    Q_DISABLE_COPY(DapServiceClientMessage)
public:
   explicit DapServiceClientMessage(QObject * parent = nullptr);
public slots:
    void messageBox(QString str);
};

#endif // DAPSERVICECLIENTMESSAGE_H
