#ifndef DAPSETTINGSSINGLETONPROVIDER_H
#define DAPSETTINGSSINGLETONPROVIDER_H

#include <DapSettings.h>
#include <QQmlEngine>
#include <QJSEngine>

class DapSettingsSingletonProvider : public DapSettings
{
public:
    explicit DapSettingsSingletonProvider(QObject *parent = nullptr);
public slots:
    /// Method that implements the singleton pattern for the qml layer.
    /// @param engine QML application.
    /// @param scriptEngine The QJSEngine class provides an environment for evaluating JavaScript code.
    static QObject *singletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
};

#endif // DAPSETTINGSSINGLETONPROVIDER_H
