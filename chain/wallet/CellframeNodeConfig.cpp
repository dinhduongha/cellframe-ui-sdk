#include "CellframeNodeConfig.h"
#ifdef Q_OS_WIN
#include "registry.h"
#endif

//#if defined (Q_OS_MACOS)
#include "dap_common.h"
//#endif

#include <QDebug>

CellframeNodeConfig::CellframeNodeConfig(QObject *parent) : QObject(parent)
{

}


CellframeNodeConfig *CellframeNodeConfig::instance(const QString &cfgPath)
{
    static CellframeNodeConfig _instance;
    if (!cfgPath.isEmpty())
        _instance.parseConfig(cfgPath);
    return &_instance;
}


QString CellframeNodeConfig::getDefaultCADir()
{
    qDebug() << "CellframeNodeConfig::getDefaultCADir";

#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
    return QString("/opt/%1-node/var/lib/ca").arg(DAP_BRAND_BASE_LO);
#elif defined (Q_OS_MACOS)
    char * l_username = NULL;
    exec_with_ret(&l_username,"whoami|tr -d '\n'");
    if (!l_username)
    {
        qWarning() << "Fatal Error: Can't obtain username";
        return "";
    }
    return QString("/Users/%1/Applications/Cellframe.app/Contents/Resources/var/lib/ca").arg(l_username);
#elif defined (Q_OS_WIN)
    return QString("%1/cellframe-node/var/lib/ca").arg(regWGetUsrPath());
#elif defined Q_OS_ANDROID
    return QString("/sdcard/cellframe-node/var/lib/ca");
#else
    return QString();
#endif
}

QString CellframeNodeConfig::getShareCADir()
{
    qDebug() << "CellframeNodeConfig::getShareCADir";

#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
    return QString("/opt/%1-node/share/ca").arg(DAP_BRAND_BASE_LO);
#elif defined(Q_OS_MACOS)
    char * l_username = NULL;
    exec_with_ret(&l_username,"whoami|tr -d '\n'");
    if (!l_username)
    {
        qWarning() << "Fatal Error: Can't obtain username";
        return "";
    }
    return QString("/Users/%1/Applications/Cellframe.app/Contents/Resources/share/ca").arg(l_username);
#elif defined (Q_OS_WIN)
    return QString("%1/cellframe-node/share/ca").arg(regWGetUsrPath());
#elif defined Q_OS_ANDROID
    return QString("/sdcard/cellframe-node/share/ca");
#else
    return QString();
#endif
}

bool CellframeNodeConfig::parseConfig(const QString &cfgPath)
{
    Q_UNUSED(cfgPath);
    //dap_config_open
    return true;
}





