#include "DapVpnOrder.h"

QStringList DapVpnOrder::s_types = {
    QString(""),
    QString("BYTE"),
    QString("MEGABYTE"),
    QString("GIGABYTE"),
    QString("SECOND"),
    QString("MINUTE"),
    QString("HOUR"),
    QString("DAY"),
    QString("MONTH")
};
QStringList DapVpnOrder::a_types = {
    QString(""),
    QString("byte"),
    QString("Mb"),
    QString("Gb"),
    QString("sec"),
    QString("min"),
    QString("h"),
    QString("D"),
    QString("M")
};

DapVpnOrder::DapVpnOrder(QObject *parent)
    : QObject(parent)
{
}

DapVpnOrder::DapVpnOrder(const DapVpnOrder &aOrder):
    QObject ( aOrder.parent()),
//    m_name(aOrder.m_name),
//    m_created(aOrder.m_created),
////    m_units(aOrder.m_units),
////    m_type(aOrder.m_type),
//    m_tokenValue(aOrder.m_tokenValue),

    m_index(aOrder.m_index),
    m_addr(aOrder.m_addr),
    m_version(aOrder.m_version),
    m_direction(aOrder.m_direction),
    m_svrUid(aOrder.m_svrUid),
    m_price(aOrder.m_price),
    m_priceUnit(aOrder.m_priceUnit),
    m_totalPrice(aOrder.m_totalPrice),
    m_nodeAddr(aOrder.m_nodeAddr),
    m_nodeLocation(aOrder.m_nodeLocation),
    m_ext(aOrder.m_ext),
    m_network(aOrder.m_network)
{

}

DapVpnOrder &DapVpnOrder::operator=(const DapVpnOrder &aOrder)
{
    if (aOrder.m_addr != m_addr) {
//        m_name = aOrder.m_name;
//        m_created = aOrder.m_created;
////        m_units = aOrder.m_units;
////        m_type = aOrder.m_type;
//        m_tokenValue = aOrder.m_tokenValue;

        m_index = aOrder.m_index;
        m_addr = aOrder.m_addr;
        m_version = aOrder.m_version;
        m_direction = aOrder.m_direction;
        m_svrUid = aOrder.m_svrUid;
        m_price = aOrder.m_price;
        m_priceUnit = aOrder.m_priceUnit;
        m_nodeAddr = aOrder.m_nodeAddr;
        m_nodeLocation = aOrder.m_nodeLocation;
        m_ext = aOrder.m_ext;
        m_network = aOrder.m_network;
        m_totalPrice = aOrder.m_totalPrice;
    }
    return (*this);
}

//QString DapVpnOrder::name() const
//{
//    return m_name;
//}

//void DapVpnOrder::setName(const QString &a_name)
//{
//    if (a_name != m_name) {
//        m_name = a_name;
//        emit nameChanged(m_name);
//    }
//}

//QDateTime DapVpnOrder::date() const
//{
//    return m_created;
//}

//void DapVpnOrder::setDate(const QDateTime &a_date)
//{
//    if (a_date != m_created) {
//        m_created = a_date;
//        emit dateChanged(m_created);
//    }
//}

//int DapVpnOrder::units() const
//{
//    return m_units;
//}

//void DapVpnOrder::setUnits(int a_units)
//{
//    if (a_units != m_units) {
//        m_units = a_units;
//        emit unitsChanged(m_units);
//    }
//}

//QString DapVpnOrder::type() const
//{
//    return s_types.takeAt(m_type);
//}

//void DapVpnOrder::setType(const QString &a_type)
//{
//    if (a_type != type())
//        return;

//    int i = 0;
//    for (auto str : s_types) {
//        if (str == a_type) {
//            m_type = DapVpnOrder::UnitType(i);
//            emit typeChanged(type());
//            break;
//        }
//        ++i;
//    }
//}

//void DapVpnOrder::setTokenValue(const DapTokenValue &a_tokenValue)
//{
//    if (&m_tokenValue == &a_tokenValue) return;
//    m_tokenValue = a_tokenValue;
//}

QString DapVpnOrder::index() const
{
    return m_index;
}

void DapVpnOrder::setIndex(const QString &a_index)
{
    if (a_index != m_index) {
        m_index = a_index;
        emit indexChanged(m_index);
    }
}

QString DapVpnOrder::totalPrice() const
{
    return m_totalPrice;
}

void DapVpnOrder::setTotalPrice(const QString &a_priceUnit)
{
    int i = 0;
    for (auto str : s_types)
    {
        if (str == a_priceUnit)
        {
            QString a_totalPrice;
            a_totalPrice = m_price + " " + a_types[i];
            if(a_totalPrice != m_totalPrice)
            {
                m_totalPrice = a_totalPrice;
                emit totalPriceChanged(m_totalPrice);
                break;
            }
        }
        ++i;
    }
}


QString DapVpnOrder::addr() const
{
    return m_addr;
}

void DapVpnOrder::setAddr(const QString &a_addr)
{
    if (a_addr != m_addr) {
        m_addr = a_addr;
//        emit nameChanged(m_name);
    }
}

int DapVpnOrder::version() const
{
    return m_version;
}

void DapVpnOrder::setVersion(const int &a_version)
{
    if (a_version != m_version) {
        m_version = a_version;
//        emit unitsChanged(m_units);
    }
}

QString DapVpnOrder::direction() const
{
    return m_direction;
}

void DapVpnOrder::setDirection(const QString &a_direction)
{
    if (a_direction != m_direction) {
        m_direction = a_direction;
//        emit nameChanged(m_name);
    }
}

QString DapVpnOrder::srvUid() const
{
    return m_svrUid;
}

void DapVpnOrder::setSrvUid(const QString &a_svrUid)
{
    if (a_svrUid != m_svrUid) {
        m_svrUid = a_svrUid;
//        emit nameChanged(m_name);
    }
}

QString DapVpnOrder::price() const
{
    return m_price;
}

void DapVpnOrder::setPrice(const QString &a_price)
{
    if (a_price != m_price) {
        m_price = a_price;
        emit priceChanged(m_price);
    }
}

QString DapVpnOrder::priceUnit() const
{
    return m_priceUnit;
}

void DapVpnOrder::setPriceUnit(const QString &a_priceUnit)
{
    if (a_priceUnit != m_priceUnit) {
        m_priceUnit = a_priceUnit;
        emit priceUnitChanged(m_priceUnit);
    }
}

QString DapVpnOrder::nodeAddr() const
{
    return m_nodeAddr;
}

void DapVpnOrder::setNodeAddr(const QString &a_nodeAddr)
{
    if (a_nodeAddr != m_nodeAddr) {
        m_nodeAddr = a_nodeAddr;
        emit nodeAddrChanged(m_nodeAddr);
    }
}

QString DapVpnOrder::nodeLocation() const
{
    return m_nodeLocation;
}

void DapVpnOrder::setNodeLocation(const QString &a_nodeLocation)
{
    if (a_nodeLocation != m_nodeLocation) {
        m_nodeLocation = a_nodeLocation;
        emit locationChanged(m_nodeLocation);
    }
}

QString DapVpnOrder::ext() const
{
    return m_ext;
}

void DapVpnOrder::setExt(const QString &a_ext)
{
    if (a_ext != m_ext) {
        m_ext = a_ext;
//        emit nameChanged(m_name);
    }
}

QString DapVpnOrder::network() const
{
    return m_network;
}

void DapVpnOrder::setNetwork(const QString &a_network)
{
    if (a_network != m_network) {
        m_network = a_network;
        emit networkChanged(m_network);
    }
}

QDataStream& operator << (QDataStream& aOut, const DapVpnOrder& aOrder)
{
     aOut   << aOrder.m_index
            << aOrder.m_addr
            << aOrder.m_version
            << aOrder.m_direction
            << aOrder.m_svrUid
            << aOrder.m_price
            << aOrder.m_priceUnit
            << aOrder.m_totalPrice
            << aOrder.m_nodeAddr
            << aOrder.m_nodeLocation
            << aOrder.m_ext
            << aOrder.m_network;


    return aOut;
}

QDataStream& operator >> (QDataStream& aIn, DapVpnOrder& aOrder)
{
        aIn >> aOrder.m_index;
        aIn.setFloatingPointPrecision(QDataStream::DoublePrecision);
        aIn >> aOrder.m_addr
            >> aOrder.m_version
            >> aOrder.m_direction
            >> aOrder.m_svrUid
            >> aOrder.m_price
            >> aOrder.m_priceUnit
            >> aOrder.m_totalPrice
            >> aOrder.m_nodeAddr
            >> aOrder.m_nodeLocation
            >> aOrder.m_ext
            >> aOrder.m_network;


    return aIn;
}

bool operator ==(const DapVpnOrder &aOrderFirst, const DapVpnOrder &aOrderSecond)
{
    return aOrderFirst.m_index == aOrderSecond.m_index;
}
