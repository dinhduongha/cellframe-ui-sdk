#include <QtDebug>
#include "DapNetwork.h"
#include "DapNetworksList.h"

/**
 * @brief DapNetworksList::DapNetworksList
 * @param parent
 */
DapNetworksList::DapNetworksList(QObject *parent) : QObject(parent)
{

}

/**
 * @brief DapNetworksList::addIfNotExist
 * @param a_name
 * @return
 */
DapNetwork * DapNetworksList::addIfNotExist(const QString& a_name)
{
    DapNetwork * network = new DapNetwork(a_name,this);
    m_list[a_name] = network;
    return network;
}

/**
 * @brief DapNetworksList::fill
 * @param networkList
 */
void DapNetworksList::fill(const QVariant& networkList)
{

}

void DapNetworksList::setNetworkProperties(const QVariantMap& a_networkProps)
{
}
