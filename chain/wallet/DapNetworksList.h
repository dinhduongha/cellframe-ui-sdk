#ifndef DAPNETWORKSLIST_H
#define DAPNETWORKSLIST_H

#include <QObject>
#include <QList>
#include <QMap>
#include <QVariantMap>
class DapNetwork;
class DapNetworksList : public QObject
{
    Q_OBJECT
    QMap<QString, DapNetwork*> m_list;
    QVariantMap m_props;
public:
    explicit DapNetworksList(QObject *parent = nullptr);
    DapNetwork * addIfNotExist(const QString& a_name);
signals:
    void networkAdded(DapNetwork* a_network);
public slots:
    void fill(const QVariant& networkList);
    void setNetworkProperties(const QVariantMap& a_networkProps);
};

#endif // DAPNETWORKSLIST_H
