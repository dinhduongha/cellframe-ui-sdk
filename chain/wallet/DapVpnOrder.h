#ifndef DAPVPNORDER_H
#define DAPVPNORDER_H

#include <QObject>
#include <QString>
#include <QDateTime>
#include "DapToken.h"
#include "DapTokenValue.h"
#include <QDataStream>
#include <QDebug>

class DapVpnOrder : public QObject
{
    Q_OBJECT
public:
//    enum UnitType {
//        Unknown,
//        Bytes,
//        Megabytes,
//        Gigabytes,
//        Seconds,
//        Minutes,
//        Hours,
//        Days,
//        Month
//    };
//    Q_ENUM(UnitType)

    Q_INVOKABLE explicit DapVpnOrder(QObject *parent = nullptr);
    Q_INVOKABLE DapVpnOrder(const DapVpnOrder& aOrder);
    Q_INVOKABLE DapVpnOrder& operator=(const DapVpnOrder& aOrder);

//    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
//    Q_PROPERTY(QDateTime date READ date WRITE setDate NOTIFY dateChanged)
//    Q_PROPERTY(int units READ units WRITE setUnits NOTIFY unitsChanged)
//    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
//    Q_PROPERTY(DapTokenValue* tokenValue READ tokenValue CONSTANT)

    Q_PROPERTY(QString Index MEMBER m_index READ index WRITE setIndex NOTIFY indexChanged)

    Q_PROPERTY(QString Location MEMBER m_nodeLocation READ nodeLocation WRITE setNodeLocation NOTIFY locationChanged)
    Q_PROPERTY(QString Network MEMBER m_network READ network WRITE setNetwork NOTIFY networkChanged)
    Q_PROPERTY(QString AddrNode MEMBER m_nodeAddr READ nodeAddr WRITE setNodeAddr NOTIFY nodeAddrChanged)
//    Q_PROPERTY(QString Price MEMBER m_price READ price WRITE setPrice NOTIFY priceChanged)
//    Q_PROPERTY(QString PriceUnit MEMBER m_priceUnit READ priceUnit WRITE setPriceUnit NOTIFY priceUnitChanged)
    Q_PROPERTY(QString TotalPrice MEMBER m_totalPrice READ totalPrice WRITE setTotalPrice NOTIFY totalPriceChanged)

    friend QDataStream& operator << (QDataStream& aOut, const DapVpnOrder& aOrder);
    friend QDataStream& operator >> (QDataStream& aOut, DapVpnOrder& aOrder);

    friend bool operator == (const DapVpnOrder &aOrderFirst, const DapVpnOrder &aOrderSecond);

//    static DapVpnOrder fromVariant(const QVariant& aOrder);

//    QString name() const;
//    void setName(const QString &a_name);
//    QDateTime date() const;
//    void setDate(const QDateTime &a_date);
//    int units() const;
//    void setUnits(int a_units);
//    QString type() const;
//    void setType(const QString &a_type);
//    DapTokenValue* tokenValue() { return &m_tokenValue; }
//    void setTokenValue(const DapTokenValue& a_tokenValue);

    QString index() const;
    void setIndex(const QString &a_index);
    QString addr() const;
    void setAddr(const QString &a_addr);
    int version() const;
    void setVersion(const int &a_version);
    QString direction() const;
    void setDirection(const QString &a_direction);
    QString srvUid() const;
    void setSrvUid(const QString &a_svrUid);
    QString price() const;
    void setPrice(const QString &a_price);
    QString priceUnit() const;
    void setPriceUnit(const QString &a_priceUnit);
    QString nodeAddr() const;
    void setNodeAddr(const QString &a_nodeAddr);
    QString nodeLocation() const;
    void setNodeLocation(const QString &a_nodeLocation);
    QString ext() const;
    void setExt(const QString &a_network);
    QString network() const;
    void setNetwork(const QString &a_network);
    QString totalPrice()const;
    void setTotalPrice(const QString &a_totalPrice);


signals:
//    void nameChanged(const QString& a_name);
//    void dateChanged(const QDateTime& a_date);
//    void unitsChanged(int a_units);
//    void typeChanged(const QString& a_type);
    void locationChanged(const QString& a_nodeLocation);
    void networkChanged(const QString& a_network);
    void nodeAddrChanged(const QString& a_nodeAddr);
    void priceChanged(const QString& a_price);
    void priceUnitChanged(const QString& a_priceUnit);
    void indexChanged(const QString &a_index);
    void totalPriceChanged(const QString &a_totalPrice);

private:
    static QStringList s_types;
    static QStringList a_types;
//    QString   m_name;
//    QDateTime m_created;
//    int       m_units = 0;
//    UnitType  m_type = UnitType::Unknown;
//    DapTokenValue m_tokenValue;

    QString   m_index;
    QString   m_addr;
    int       m_version;
    QString   m_direction;
    QString   m_svrUid;
    QString   m_price;
    QString   m_priceUnit;
    QString   m_totalPrice;
    QString   m_nodeAddr;
    QString   m_nodeLocation;
    QString   m_ext;
    QString   m_network;
};
Q_DECLARE_METATYPE(DapVpnOrder);
#endif // DAPVPNORDER_H
