#include "DapGetListOdersCommand.h"

/// Overloaded constructor.
/// @param asServiceName Service name.
/// @param parent Parent.
/// @details The parent must be either DapRPCSocket or DapRPCLocalServer.
/// @param asCliPath The path to cli nodes.
DapGetListOrdersCommand::DapGetListOrdersCommand(const QString &asServicename, QObject *parent, const QString &asCliPath)
    : DapAbstractCommand(asServicename, parent, asCliPath)
{

}

/// Send a response to the client.
/// @details Performed on the service side.
/// @param arg1...arg10 Parameters.
/// @return Reply to client.
QVariant DapGetListOrdersCommand::respondToClient(const QVariant &arg1, const QVariant &arg2, const QVariant &arg3,
                                              const QVariant &arg4, const QVariant &arg5, const QVariant &arg6,
                                              const QVariant &arg7, const QVariant &arg8, const QVariant &arg9,
                                              const QVariant &arg10)
{
    Q_UNUSED(arg1)
    Q_UNUSED(arg2)
    Q_UNUSED(arg3)
    Q_UNUSED(arg4)
    Q_UNUSED(arg5)
    Q_UNUSED(arg6)
    Q_UNUSED(arg7)
    Q_UNUSED(arg8)
    Q_UNUSED(arg9)
    Q_UNUSED(arg10)

    qDebug() << "DapGetListOrdersCommand::respondToClient";

    QList<DapVpnOrder> orders;
    QStringList listNetworks;
#if defined(Q_OS_LINUX) || defined (Q_OS_MACOS)
    QProcess processN;
    QString command = QString("%1 net list").arg(m_sCliPath);
    qInfo() << "command:" << command;
    processN.start(command);
    processN.waitForFinished(-1);
    QString result = QString::fromLatin1(processN.readAll());
    qInfo() << "result:" << result;
    result.remove(' ');
    result.remove("\r");
    result.remove("Networks:");
    if(!(result.isEmpty() || result.isNull() || result.contains('\'')))
    {
        listNetworks = result.split("\n", QString::SkipEmptyParts);
    }
    qDebug() << "list" << listNetworks;

    for(int itr_network = 0 ; itr_network < listNetworks.length(); itr_network++)
    {
        QProcess process;
        QString command = QString("%1 net_srv -net %2 order find").arg(m_sCliPath).arg(listNetworks[itr_network]);
        qInfo() << "command:" << command;
        process.start(command);
        process.waitForFinished(-1);
        QString res = QString::fromLocal8Bit(process.readAll());
        qInfo() << "result:" << res;
        res.remove("\r");
        QStringList res_split = res.split("== ");
        res_split.removeFirst();
        for(int itr_orders = 0; itr_orders < res_split.length(); itr_orders++ )
        {
            DapVpnOrder order;
            res_split[itr_orders].remove(' ');
            QByteArray orders_bytes = res_split[itr_orders].toUtf8();

//            QRegularExpression regex(R"(^version: (\S+)\nlocation: (\S+)\nprice: )");
//            QRegularExpressionMatch match = regex.match(orders_bytes);
//            QString text_1 = match.captured(1);
//            QString text_2 = match.captured(2);
//            QString text_3 = match.captured(3);
//            QString text_4 = match.captured(4);

            QRegularExpression addr("Order(.+)", QRegularExpression::MultilineOption);
            QRegularExpressionMatch m_addr = addr.match(orders_bytes);

            QRegularExpression version("version:(.+)", QRegularExpression::MultilineOption);
            QRegularExpressionMatch m_ver = version.match(orders_bytes);

            QRegularExpression location("location:(.+)", QRegularExpression::MultilineOption);
            QRegularExpressionMatch m_loc = location.match(orders_bytes);

            QRegularExpression price("price:(.+)", QRegularExpression::MultilineOption);
            QRegularExpressionMatch m_price = price.match(orders_bytes);

            QStringList splitPrice1 = m_price.captured(1).split("(");
            QStringList splitPrice2 = splitPrice1[1].split(")");
            QString price_str =  splitPrice2[0];

            QRegularExpression direction("direction:(.+)", QRegularExpression::MultilineOption);
            QRegularExpressionMatch m_direction = direction.match(orders_bytes);

            QRegularExpression srvUid("srv_uid:(.+)", QRegularExpression::MultilineOption);
            QRegularExpressionMatch m_srvUid = srvUid.match(orders_bytes);

            QRegularExpression priceUnit("price_unit:(.+)", QRegularExpression::MultilineOption);
            QRegularExpressionMatch m_priceUnit = priceUnit.match(orders_bytes);

            QRegularExpression nodeAddr("node_addr:(.+)", QRegularExpression::MultilineOption);
            QRegularExpressionMatch m_nodeAddr = nodeAddr.match(orders_bytes);

            QRegularExpression ext("ext:(.+)", QRegularExpression::MultilineOption);
            QRegularExpressionMatch m_ext = ext.match(orders_bytes);

            QString orderNetwork = listNetworks[itr_network];

            order.setIndex(QString::number(orders.length()+1));
            order.setAddr(m_addr.captured(1));
            order.setVersion(m_ver.captured(1).toInt());
            order.setPrice(price_str);
            order.setNetwork(orderNetwork);
            order.setNodeLocation(m_loc.captured(1));
            order.setNodeAddr(m_nodeAddr.captured(1));
            order.setSrvUid(m_srvUid.captured(1));
            order.setPriceUnit(m_priceUnit.captured(1));
            order.setExt(m_ext.captured(1));
            order.setDirection(m_direction.captured(1));
            order.setTotalPrice(m_priceUnit.captured(1));

            orders.append(order);
        }
    }
#endif
    QByteArray datas;
    QDataStream out(&datas, QIODevice::WriteOnly);
    out << orders;

    return QJsonValue::fromVariant(datas.toHex());
}

/// Reply from service.
/// @details Performed on the service side.
/// @return Service reply.
QVariant DapGetListOrdersCommand::replyFromService()
{
    DapRpcServiceReply *reply = static_cast<DapRpcServiceReply *>(sender());

    emit serviceResponded(reply->response().toJsonValue().toVariant().toByteArray());

    return reply->response().toJsonValue().toVariant();
}
