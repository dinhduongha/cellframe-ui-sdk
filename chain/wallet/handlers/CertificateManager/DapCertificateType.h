#ifndef DAPCERTIFICATETYPE_H
#define DAPCERTIFICATETYPE_H


#include <QObject>


class DapCertificateType
{
    Q_GADGET
public:
    //certificate type access
    enum AccessKeyType{
        Public = 0,
        PublicAndPrivate = 1, // this type is called "Private" !!!
        Both = 2, // used only for filtering !!!
        Error = -1
    };
    Q_ENUM(AccessKeyType);

    //certificate location dir
    enum DirType{
        DefaultDir = 0,
        ShareDir
    };
    Q_ENUM(DirType);


protected:
    explicit DapCertificateType() {  }
};



#endif // DAPCERTIFICATETYPE_H
