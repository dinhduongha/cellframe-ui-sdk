#include <QtDebug>
#include "DapGetWalletsInfoCommand.h"

/// Overloaded constructor.
/// @param asServiceName Service name.
/// @param parent Parent.
/// @details The parent must be either DapRPCSocket or DapRPCLocalServer.
/// @param asCliPath The path to cli nodes.
DapGetWalletsInfoCommand::DapGetWalletsInfoCommand(const QString &asServicename, QObject *parent, const QString &asCliPath)
    : DapAbstractCommand(asServicename, parent, asCliPath)
{

}

/// Send a response to the client.
/// @details Performed on the service side.
/// @param arg1...arg10 Parameters.
/// @return Reply to client.
QVariant DapGetWalletsInfoCommand::respondToClient(const QVariant &arg1, const QVariant &arg2, const QVariant &arg3,
                                              const QVariant &arg4, const QVariant &arg5, const QVariant &arg6,
                                              const QVariant &arg7, const QVariant &arg8, const QVariant &arg9,
                                              const QVariant &arg10)
{
    Q_UNUSED(arg1)
    Q_UNUSED(arg2)
    Q_UNUSED(arg3)
    Q_UNUSED(arg4)
    Q_UNUSED(arg5)
    Q_UNUSED(arg6)
    Q_UNUSED(arg7)
    Q_UNUSED(arg8)
    Q_UNUSED(arg9)
    Q_UNUSED(arg10)

    qDebug() << "DapGetWalletsInfoCommand::respondToClient";

    QList<DapWallet> wallets;

    QStringList list;
    QProcess processN;
    QString command = QString("%1 net list").arg(m_sCliPath);
    qInfo() << "command:" << command;
    processN.start(command);
    processN.waitForFinished(-1);
    QString result = QString::fromLatin1(processN.readAll());
    qInfo() << "result:" << result;
    result.remove(' ');
    result.remove("\r");
    result.remove("Networks:");
    if(!(result.isEmpty() || result.isNull() || result.contains('\'')))
    {
        list = result.split("\n", QString::SkipEmptyParts);
    }
    qDebug() << "list" << list;

    QProcess process;
    command = QString("%1 wallet list").arg(m_sCliPath);
    qInfo() << "command:" << command;
    process.start(command);
    process.waitForFinished(-1);
    QString res = QString::fromLocal8Bit(process.readAll());
    qInfo() << "result:" << res;
    res.remove("\r");
    QRegularExpression rx("wallet:\\s(.+)\\s", QRegularExpression::MultilineOption);
    QRegularExpressionMatchIterator itr = rx.globalMatch(res);
    while (itr.hasNext())
    {
        QRegularExpressionMatch match = itr.next();
        QString walletName = match.captured(1);
        DapWallet wallet;
        wallet.setName(walletName);
        auto begin = list.begin();
        auto end = list.end();
        for(; begin != end; ++begin)
        {
            qDebug() << "Wallet" << walletName << "added network:" << *begin;
            QProcess process_token;
            command = QString("%1 wallet info -w %2 -net %3")
                    .arg(m_sCliPath)
                    .arg(walletName)
                    .arg(*begin);
            qInfo() << "command:" << command;
            process_token.start(command);

            process_token.waitForFinished(-1);
            QByteArray result_tokens = process_token.readAll();
            qInfo() << "result:" << result_tokens;

            //Name:
#ifdef Q_OS_WIN
            QRegularExpression regex(R"(^wallet: (\S+)\r\naddr: (\S+)\r\nnetwork: (\S+)\r\nbalance:)");
#else
            QRegularExpression regex(R"(^wallet: (\S+)\naddr: (\S+)\nnetwork: (\S+)\nbalance:)");
#endif
            QRegularExpressionMatch match = regex.match(result_tokens);
            if (!match.hasMatch())
            {
                qWarning() << "Can't parse result" << result_tokens;
            }
            else
            {
                QString network = match.captured(3);
                if (network != *begin)
                    continue;

                wallet.addNetwork(*begin);

                wallet.addAddress(match.captured(2), *begin);
                qDebug() << "Wallet" << walletName << "address" << match.captured(2)
                         << "network" << match.captured(3);

                QRegularExpression balanceRegex(R"((\d+.\d+) \((\d+)\) (\w+))");
                QRegularExpressionMatchIterator matchIt = balanceRegex.globalMatch(result_tokens);

                while (matchIt.hasNext())
                {
                    QRegularExpressionMatch match = matchIt.next();
                    qDebug() << "Token:" << match.captured(3)
                             << match.captured(1) << match.captured(2);

                    DapWalletToken *token = new DapWalletToken();
                    token->setName(match.captured(3));
                    token->setBalance(match.captured(1).toDouble());
                    token->setEmission(match.captured(2).toULongLong());
                    token->setNetwork(*begin);

                    wallet.addToken(token);
                }

                /*
                    token = new DapWalletToken();
                    token->setName(regex.cap(7).trimmed());
                    token->setBalance(regex.cap(5).toDouble());
                    QString str = regex.cap(6);
                    token->setEmission(regex.cap(8).toULongLong());
                    token->setNetwork(*begin);
                    wallet.addToken(token);*/
            }

        }
        wallets.append(wallet);
    }

    QByteArray datas;
    QDataStream out(&datas, QIODevice::WriteOnly);
    out << wallets;

    return QJsonValue::fromVariant(datas.toHex());
}


/// Reply from service.
/// @details Performed on the service side.
/// @return Service reply.
QVariant DapGetWalletsInfoCommand::replyFromService()
{
    DapRpcServiceReply *reply = static_cast<DapRpcServiceReply *>(sender());

    emit serviceResponded(reply->response().toJsonValue().toVariant().toByteArray());

    return reply->response().toJsonValue().toVariant();
}
